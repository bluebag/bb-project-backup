Project Backup
=========

This role enables the sync of files to S3
It creates the following:
 -  A sync script in the scripts folder of the project
 -  A cron job to run the script
 -  A logrotate job to rotate the cron log


It uses a loop in the cron template to add all jobs to one cron file.

Requirements
------------

Requires the S3 and S3Cmd roles

Role Variables
--------------

A dict var of the folders to sync
Typically thee will be the Drupal sites/default/files folder

@to_do: use an exclude file to ignore certain patterns.

Dependencies
------------

Requires the S3 and S3Cmd roles

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: project_backup, when: s3_sync_backup }

License
-------

BSD

Author Information
------------------

George Boobyer
